---
title: "Halloween 4: O Retorno de Michael Myers"
date: 2021-10-19T01:45:32-03:00
draft: false
id: 4
release: 1988
rottentomatoes: 29
metacritic: 34
poster: "https://www.themoviedb.org/t/p/original/9ahPOvn7YbqdTnUg3MpE97YjJQt.jpg"
trailer: "https://www.youtube.com/embed/rfvBru3MKsg"
synopsis: "Ten years ago, on the night of October 31st, a small Midwestern town fell victim to an escaped killer. Under the cover of darkness, he carried out the most horrifying mass murder on record… Tonight He’s back!"
---