---
title: "Halloween III: A Noite das Bruxas"
date: 2021-10-19T01:45:31-03:00
draft: false
id: 3
release: 1982
rottentomatoes: 39
metacritic: 50
poster: "https://www.themoviedb.org/t/p/original/beCi8tfUDcvas4QAzv2hBjheNVn.jpg"
trailer: "https://www.youtube.com/embed/ZuHlHbHbbhI"
synopsis: "HALLOWEEN III Season of the Witch takes a break from the Michael Myers storyline. As Halloween approaches Silver Shamrock Novelties is planning something big and very evil… An ancient evil."
---