---
title: "Halloween"
date: 2021-10-19T01:45:39-03:00
draft: false
id: 11
release: 2018
rottentomatoes: 79
metacritic: 67
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/bXs0zkv2iGVViZEy78teg2ycDBm.jpg"
trailer: "https://www.youtube.com/embed/PPFqbdEoJFQ"
synopsis: "Jamie Lee Curtis returns to her iconic role as Laurie Strode, who comes to her final confrontation with Michael Myers, the masked figure who has haunted her since she narrowly escaped his killing spree on Halloween night four decades ago."
---