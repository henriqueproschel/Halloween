---
title: "Halloween Kills: O Terror Continua"
date: 2021-10-19T01:45:40-03:00
draft: false
id: 12
release: 2021
rottentomatoes: 39
metacritic: 42
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/5zPNs90yGHKdKadN2Bz32Pug8jY.jpg"
trailer: "https://www.youtube.com/embed/9lqYjItEyOU"
synopsis: "Laurie Strode believes she has finally won the fight against Michael Myers, but she is surprised by his return. Determined to put an end to her attacks, she and other survivors decide to face him and end the cycle once and for all."
---