---
title: "Halloween: A Noite do Terror"
date: 2021-10-19T01:45:29-03:00
draft: false
id: 1
release: 1978
rottentomatoes: 96
metacritic: 87
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/qVpCaBcnjRzGL3nOPHi6Suy0sB6.jpg"
trailer: "https://www.youtube.com/embed/emN1KbD_DxM"
synopsis: "Halloween night, 1963. A six year old child murdered his 17-year-old sister. He was locked away at Smith’s Grove-Warren Sanitarium for 15 years, but on October 30, 1978, things were about to change."
---