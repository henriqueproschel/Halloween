---
title: "Halloween Ends"
date: 2021-10-19T01:45:41-03:00
draft: false
id: 13
release: 2022
rottentomatoes: "ND"
metacritic: "ND"
poster: "https://www.themoviedb.org/t/p/original/aGbG5mKbY7R7vltSbKrY2f40BL4.jpg"
trailer: "https://www.youtube.com/embed/7skIwTA8e90"
synopsis: "The saga of Michael Myers and Laurie Strode continues in the next thrilling chapter of the Halloween series."
---