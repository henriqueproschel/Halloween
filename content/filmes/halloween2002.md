---
title: "Halloween: Ressurreição"
date: 2021-10-19T01:45:36-03:00
draft: false
id: 8
release: 2002
rottentomatoes: 12
metacritic: 19
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/iIc41W2AViZWasmvWEHOTiW8lym.jpg"
trailer: "https://www.youtube.com/embed/JoJi4Jb-r2Y"
synopsis: "It’s been 3 years since Laurie Strode killed Michael Myers on Halloween … or so we thought. Myers switched places with a paramedic and instead of killing her brother, Laurie killed an innocent man."
---