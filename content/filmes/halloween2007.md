---
title: "Halloween: O Início"
date: 2021-10-19T01:45:37-03:00
draft: false
id: 9
release: 2007
rottentomatoes: 28
metacritic: 47
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/kX0tl6NyfLw1N2vegNEVO0SNZAg.jpg"
trailer: "https://www.youtube.com/embed/MhyZmUeq6do"
synopsis: "In Haddonfield, nine year old Michael Myers is being raised by a dysfunctional family. His mother is a stripper, his foster father is an abusive alcoholic and his sister is promiscuous. He does, however, have a strong affinity toward his baby sibling Laurie."
---