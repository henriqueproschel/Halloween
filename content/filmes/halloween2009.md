---
title: "Halloween II"
date: 2021-10-19T01:45:38-03:00
draft: false
id: 10
release: 2009
rottentomatoes: 22
metacritic: 35
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/vSHPM4LQDpWdQrD5KZWK6wNqSOD.jpg"
trailer: "https://www.youtube.com/embed/hKZT0WF6V0Q"
synopsis: "Taking place one year later, Laurie is now living with Sheriff Brackett and his family. Michael’s body has been missing since last Halloween and Laurie has been having recurring nightmares about him."
---