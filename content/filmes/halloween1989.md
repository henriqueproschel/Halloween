---
title: "Halloween 5: A Vingança de Michael Myers"
date: 2021-10-19T01:45:33-03:00
draft: false
id: 5
release: 1989
rottentomatoes: 12
metacritic: 28
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/m4mVpkghEgWrZsxpgCmpDMlNrL2.jpg"
trailer: "https://www.youtube.com/embed/NM1UX616SCM"
synopsis: "Michael managed to crawl out of the mine, and takes refuge with a hermit. A year later, October 31st arrives, and Jamie Lloyd, his niece, is having nightmares about him. She seems to now have a telepathic bond with her evil uncle."
---