---
title: "Halloween II: O Pesadelo Continua"
date: 2021-10-19T01:45:30-03:00
draft: false
id: 2
release: 1981
rottentomatoes: 31
metacritic: 40
poster: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/xyBX7rFmNB4nM2RBW3042RQPFG2.jpg"
trailer: "https://www.youtube.com/embed/vzOdUKVD8Ac"
synopsis: "Picking up right where the first HALLOWEEN left off, HALLOWEEN II continues the night HE came home. Michael Myers is still alive and Dr. Loomis is searching the streets of Haddonfield to find him."
---
