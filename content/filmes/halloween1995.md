---
title: "Halloween: A Maldição de Michael Myers"
date: 2021-10-19T01:45:34-03:00
draft: false
id: 6
release: 1995
rottentomatoes: 9
metacritic: 10
poster: "https://www.themoviedb.org/t/p/original/upxeq04VR9sUylUmMzpWNpy9PlM.jpg"
trailer: "https://www.youtube.com/embed/NaO24xfCwW8"
synopsis: "It’s been six years since the town of Haddonfield last heard from Michael Myers, Jamie Lloyd, and the Man in Black. Dr. Loomis has given up his practice, and retired to the countryside."
---