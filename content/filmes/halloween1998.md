---
title: "Halloween H20: 20 Anos Depois"
date: 2021-10-19T01:45:35-03:00
draft: false
id: 7
release: 1998
rottentomatoes: 52
metacritic: 52
poster: "https://www.themoviedb.org/t/p/original/8OOyralXanVmWxPvYFljiVCQoGS.jpg"
trailer: "https://www.youtube.com/embed/-WTT_WYMLVw"
synopsis: "It’s been Twenty years since Michael Myers returned to Haddonfield to kill Laurie Strode. Since that night Laurie has changed her name and moved to California where she is the head mistress of the Hillcrest Academy."
---